var board = new Board()
var hand1 = new Hand()
var hand2 = new Hand()
var tile = new Tile(2, 2, false, 2)
var player1Position = [0, 0]
var player2Position = [6, 6]
var playerTurn = true
var stage = true

function handDealer() {
    var deck = []
    for(i = 1; i < 25; i++) {
        deck.push(i)
    }
    deck.sort(function(a, b) {return 0.5 - Math.random()})
    for(i = 0; i < 12; i++) {
        hand1.addCard(deck[i])
    }
    for(i = 12; i < 24; i++) {
        hand2.addCard(deck[i])
    }
    hand1.drawCard()
    hand2.drawCard()
}

function clearTreasure() {
    imgs = document.getElementsByClassName("treasure")
    for (var i = 0; i < imgs.length; i++) {
        var img = imgs[i]
        if (img)
            img.parentElement.removeChild(img)
    }
}

function checkTreasure() {
    if (playerTurn) {
        var tile = board.board[player1Position[0]][player1Position[1]]
        if (tile.treasure === hand1.currentCard) {
            tile.treasure = null
            hand1.drawCard()
            draw()
        }
    } else {
        var tile = board.board[player2Position[0]][player2Position[1]]
        if (tile.treasure === hand2.currentCard) {
            tile.treasure = null
            hand2.drawCard()
            draw()
        }
    }
}

function checkWin() {
    if (player1Position[0] === 0 && player1Position[1] === 0 && !hand1.currentCard)
        window.location.href = "redwin.html"
    else if (player2Position[0] === 6 && player2Position[1] === 6 && !hand2.currentCard)
        window.location.href = "bluewin.html"
}

function draw() {
    checkWin()
    clearTreasure()
    for (var i = 0; i < 7; i++) {
        for (var j = 0; j < 7; j++) {
            var div = document.getElementById(i.toString() + j.toString())
            var img = div.getElementsByTagName("img")[0]
            img.src = "images/" + board.board[i][j].numPaths.toString() + ".png"
            img.style.transform = "rotate(" + (board.board[i][j].rotation * 90).toString() + "deg)"
            if (board.verifyMove([i, j]) && !stage) {
                img.style.backgroundColor = "#90ee90"
            } else {
                img.style.backgroundColor = ''
            }
            if (board.board[i][j].locked && !div.classList.contains("locked"))
                div.className += " locked"
            if (board.board[i][j].treasure) {
                var treasure
                if (div.getElementsByClassName("treasure")[0]) {
                    treasure = div.getElementsByClassName("treasure")[0]
                } else {
                    treasure = document.createElement("img")
                }
                treasure.src = "images/treasures/" + board.board[i][j].treasure.toString() + ".svg"
                treasure.width = 40
                treasure.height = 40
                if (!treasure.classList.contains("treasure"))
                    treasure.className += "treasure"
                div.appendChild(treasure)
            } else {
                if (div.getElementsByClassName("treasure")[0])
                    treasure = div.removeChild(div.getElementsByClassName("treasure")[0])
            }
        }
    }
    var div = document.getElementById("outer-cell")
    var img = div.getElementsByTagName("img")[0]
    var outerTile = board.outerTile
    img.src = "images/" + outerTile.numPaths.toString() + ".png"
    img.style.transform = "rotate(" + (outerTile.rotation * 90).toString() + "deg)"
    if (outerTile.treasure) {
        var treasure
        if (div.getElementsByClassName("treasure")[0]) {
            treasure = div.getElementsByClassName("treasure")[0]
        } else {
            treasure = document.createElement("img")
        }
        treasure.src = "images/treasures/" + outerTile.treasure.toString() + ".svg"
        treasure.width = 40
        treasure.height = 40
        if (!treasure.classList.contains("treasure"))
            treasure.className += "treasure"
        div.appendChild(treasure)
    }

    var instructions = document.getElementById("instructions")
    if (stage) {
        instructions.innerText = "Place the tile"
    } else {
        instructions.innerText = "Move your peg"
    }

    var currentTreasureImg = document.getElementById("current-treasure")
    var indicator = document.getElementById("turn-indicator")
    if (playerTurn) {
        if (hand1.currentCard) {
            currentTreasureImg.hidden = false
            currentTreasureImg.src = "images/treasures/" + hand1.currentCard.toString() + ".svg"
            currentTreasureImg.style.backgroundColor = "transparent"
            currentTreasureImg.style.transform = "rotate(0deg)"
        } else {
            currentTreasureImg.src = "images/" + board.board[0][0].numPaths.toString() + ".png"
            currentTreasureImg.style.backgroundColor = "#985858"
            currentTreasureImg.style.transform = "rotate(" + (board.board[0][0].rotation * 90).toString() + "deg)"
        }

        indicator.innerText = "Red's Turn"
        indicator.style.color = "red"
    } else {
        if (hand2.currentCard) {
            currentTreasureImg.hidden = false
            currentTreasureImg.src = "images/treasures/" + hand2.currentCard.toString() + ".svg"
            currentTreasureImg.style.backgroundColor = "transparent"
            currentTreasureImg.style.transform = "rotate(0deg)"
        } else {
            currentTreasureImg.src = "images/" + board.board[6][6].numPaths.toString() + ".png"
            currentTreasureImg.style.backgroundColor = "#638caf"
            currentTreasureImg.style.transform = "rotate(" + (board.board[6][6].rotation * 90).toString() + "deg)"
        }
        indicator.innerText = "Blue's Turn"
        indicator.style.color = "blue"
    }
    var div1 = document.getElementById(player1Position[0].toString() + player1Position[1].toString())
    var div2 = document.getElementById(player2Position[0].toString() + player2Position[1].toString())
    if (document.getElementById("red")) {
        if (!div1.querySelector("#red")) {
            div1.appendChild(document.getElementById("red"))
        }
    } else {
        var player1 = document.createElement("img")
        player1.src = "images/red.svg"
        player1.width = 80
        player1.height = 80
        player1.id = "red"
        div1.appendChild(player1)
    }
    if (document.getElementById("blue")) {
        if (!div2.querySelector("#blue"))
            div2.appendChild(document.getElementById("blue"))
    } else {
        var player2 = document.createElement("img")
        player2.src = "images/blue.svg"
        player2.width = 80
        player2.height = 80
        player2.id = "blue"
        div2.appendChild(player2)
    }
}

// Mathematical Modulo Operator 
// http://javascript.about.com/od/problemsolving/a/modulobug.htm
Number.prototype.mod = function (n) {
    return ((this % n) + n) % n
}

function shiftPlayer(player, direction) {
    if (player === 1) {
        if (direction === 0)
            player1Position[0] = (player1Position[0] - 1).mod(7)
        else if (direction === 2)
            player1Position[0] = (player1Position[0] + 1).mod(7)
        else if (direction === 1)
            player1Position[1] = (player1Position[1] + 1).mod(7)
        else if (direction === 3)
            player1Position[1] = (player1Position[1] - 1).mod(7)
    }
    if (player === 2) {
        if (direction === 0)
            player2Position[0] = (player2Position[0] - 1).mod(7)
        else if (direction === 2)
            player2Position[0] = (player2Position[0] + 1).mod(7)
        else if (direction === 1)
            player2Position[1] = (player2Position[1] + 1).mod(7)
        else if (direction === 3)
            player2Position[1] = (player2Position[1] - 1).mod(7)
    }
}

window.onkeyup = function (e) {
    var key = e.keyCode ? e.keyCode : e.which
    if (key === 82) {
        board.outerTile.rotate()
    }
    draw()
}

document.addEventListener("click", function (e) {
    e = e || window.event;
    var validShifts = [01, 03, 05, 10, 16, 30, 36, 50, 56, 61, 63, 65]
    var target = e.target || e.srcElement

    if (target.parentElement.classList.contains("cell")) {
        target = target.parentElement.id
        var x = parseInt(target[0])
        var y = parseInt(target[1])
        if (stage) {
            for (i = 0; i < validShifts.length; i++) {
                if (validShifts[i] == target) {
                    if (player1Position[1] === y && x === 6) {
                        shiftPlayer(1, 0)
                    } else if (player1Position[1] === y && x === 0) {
                        shiftPlayer(1, 2)
                    } else if (player1Position[0] === x && y === 6) {
                        shiftPlayer(1, 3)
                    } else if (player1Position[0] === x && y === 0) {
                        shiftPlayer(1, 1)
                    }

                    if (player2Position[1] === y && x === 6)
                        shiftPlayer(2, 0)
                    else if (player2Position[1] === y && x === 0)
                        shiftPlayer(2, 2)
                    else if (player2Position[0] === x && y === 6)
                        shiftPlayer(2, 3)
                    else if (player2Position[0] === x && y === 0)
                        shiftPlayer(2, 1)

                    board.shiftboard(x, y)

                    stage = !stage

                    while (board.playerCanMove.length > 0)
                        board.playerCanMove.pop()
                    while (board.visited.length > 0)
                        board.visited.pop()

                    if (playerTurn) {
                        board.pathfinder(player1Position[0], player1Position[1])
                    } else {
                        board.pathfinder(player2Position[0], player2Position[1])
                    }

                    draw()
                }
            }
        } else {
            if (board.verifyMove(target)) {
                if (playerTurn) {
                    player1Position = [x, y]
                } else {
                    player2Position = [x, y]
                }
                checkTreasure()
                board.playerCanMove = []
                board.visited = []
                playerTurn = !playerTurn
                stage = !stage
                draw()
            }
        }
    }
}, true)

function main() {
    handDealer()
    draw()
}

window.onload = main