class Board {
    constructor() {
        this.board = []
        this.visited = []
        this.playerCanMove = []
        this.outerTile
        this.arrangeBoard()
    }

    getPaths(tile) {
        if (tile.numPaths == 2) {
            if (tile.rotation % 2 === 0) {
                return [true, false, true, false]
            } else {
                return [false, true, false, true]
            }
        }
        if (tile.numPaths == 3) {
            if (tile.rotation === 0) {
                return [true, true, true, false]
            }
            if (tile.rotation === 1) {
                return [false, true, true, true]
            }
            if (tile.rotation === 2) {
                return [true, false, true, true]
            }
            if (tile.rotation === 3) {
                return [true, true, false, true]
            }
        }

        if (tile.numPaths == 4) {
            if (tile.rotation === 0) {
                return [false, true, true, false]
            }
            if (tile.rotation === 1) {
                return [false, false, true, true]
            }
            if (tile.rotation === 2) {
                return [true, false, false, true]
            }
            if (tile.rotation === 3) {
                return [true, true, false, false]
            }
        }
    }

    isVisited(position) {
        var arrayLength
        arrayLength = this.visited.length
        for (var i = 0; i < arrayLength; i++) {
            if (position[0] == this.visited[i][0] && position[1] == this.visited[i][1]) {
                return true
            }
        }
        return false
    }

    canMove(startX, startY, endX, endY) {
        if (endX < 0 || endX > 6 || endY < 0 || endY > 6) {
            return false
        }
        var startTile, endTile;
        startTile = this.board[startX][startY]
        endTile = this.board[endX][endY]

        var startPaths, endPaths;
        startPaths = this.getPaths(startTile)
        endPaths = this.getPaths(endTile)

        // Tile is moving left
        if (startX === endX && startY > endY && startPaths[3] === true && endPaths[1] === true) {
            return true
        }
        // Tile is moving right
        if (startX === endX && startY < endY && startPaths[1] === true && endPaths[3] === true) {
            return true
        }
        // Tile is moving up
        if (startY === endY && startX > endX && startPaths[0] === true && endPaths[2] === true) {
            return true
        }
        // Tile is moving down
        if (startY === endY && startX < endX && startPaths[2] === true && endPaths[0] === true) {
            return true
        }
        // otherwise return fales		
        return false
    }

    pathfinder(startX, startY) {
        this.visited.push([startX, startY])
        this.playerCanMove.push([startX, startY])
        // Looking Down
        if (!this.isVisited([startX + 1, startY]) && this.canMove(startX, startY, startX + 1, startY)) {
            this.pathfinder(startX + 1, startY)
        }

        //moving up 
        if (!this.isVisited([startX - 1, startY]) && this.canMove(startX, startY, startX - 1, startY)) {
            this.pathfinder(startX - 1, startY)
        }

        //moving right 
        if (!this.isVisited([startX, startY + 1]) && this.canMove(startX, startY, startX, startY + 1)) {
            this.pathfinder(startX, startY + 1)
        }
        //moving left
        if (!this.isVisited([startX, startY - 1]) && this.canMove(startX, startY, startX, startY - 1)) {
            this.pathfinder(startX, startY - 1)
        }
    }

    verifyMove(moveAttempt) {
        // Pass in coordinates player is trying to move to and the array of coordinates that are valid move and the function will return true/false	

        var arrayLength
        arrayLength = this.playerCanMove.length //how many coordinates to check	
        for (var i = 0; i <= arrayLength - 1; i++) { //loop through each number in each coordinate in the list of valid moves
            if (moveAttempt[0] == this.playerCanMove[i][0] && moveAttempt[1] == this.playerCanMove[i][1]) { // if the first and second number of the attempted move space are in any of the valid coordinates, return true
                return true
            }
        }
        return false
    }

    shiftboard(x, y) {
        var tempTile
        if (x == 0) {
            tempTile = this.board[6][y]
            for (var i = 6; i >= 1; i--) {
                this.board[i][y] = this.board[i - 1][y]
            }
            this.board[x][y] = this.outerTile
            this.outerTile = tempTile
            draw()
        }
        if (y == 0) {
            tempTile = this.board[x][6]
            for (var i = 6; i >= 1; i--) {
                this.board[x][i] = this.board[x][i - 1]
            }
            this.board[x][y] = this.outerTile
            this.outerTile = tempTile
            draw()
        }
        if (x == 6) {
            tempTile = this.board[0][y]
            for (var i = 0; i < 6; i++) {
                this.board[i][y] = this.board[i + 1][y]
            }
            this.board[x][y] = this.outerTile
            this.outerTile = tempTile
            draw()
        }
        if (y == 6) {
            tempTile = this.board[x][0]
            for (var i = 0; i < 6; i++) {
                this.board[x][i] = this.board[x][i + 1]
            }
            this.board[x][y] = this.outerTile
            this.outerTile = tempTile
            draw()
        }
    }

    randDirection() {
        return Math.floor(Math.random() * (3 - 0 + 1) + 0)
    }

    findNextTile(twos, threes, fours) {
        var nextTile = 0;
        if (twos && threes && fours)
            nextTile = Math.floor(Math.random() * (4 - 2 + 1) + 2)
        else if (twos && threes)
            nextTile = Math.round(Math.random()) + 1
        else if (threes && fours)
            nextTile = Math.floor(Math.random() * (4 - 3 + 1) + 3)
        else if (twos && fours)
            nextTile = Math.round(Math.random()) * 2 + 2
        else if (twos)
            nextTile = 2
        else if (threes)
            nextTile = 3
        else
            nextTile = 4

        return nextTile
    }

    arrangeBoard() {
        var twosCount = 0
        var twosMax = 12
        var threesCount = 0
        var threesMax = 6
        var foursCount = 0
        var foursMax = 16
        var lockedCount = 0
        var lockedOrder = [
            [4, 0],
            [3, 1],
            [3, 1],
            [4, 1],
            [3, 0],
            [3, 0],
            [3, 1],
            [3, 2],
            [3, 0],
            [3, 3],
            [3, 2],
            [3, 2],
            [4, 3],
            [3, 3],
            [3, 3],
            [4, 2]
        ]
        var treasuresCount = 1
        var randTreasures = 0
        var randTreasureMax = 6
        var cornerCount = 0;
        for (var i = 0; i < 7; i++) {
            for (var j = 0; j < 7; j++) {
                var newTile
                if (i % 2 === 0 && j % 2 === 0) {
                    if ((i === 0 || i === 6) && (j === 0 || j === 6)) {
                        newTile = new Tile(lockedOrder[lockedCount][0], lockedOrder[lockedCount][1], cornerCount, null, true)
                        cornerCount++
                    } else {
                        newTile = new Tile(lockedOrder[lockedCount][0], lockedOrder[lockedCount][1], null, treasuresCount, true)
                        treasuresCount++
                    }
                    lockedCount++
                } else {
                    var nextTile = this.findNextTile(twosCount < twosMax, threesCount < threesMax, foursCount < foursMax)

                    if (nextTile === 2 && twosCount < twosMax) {
                        newTile = new Tile(2, this.randDirection(), null, null, false)
                        twosCount++
                    } else if (nextTile === 3 && threesCount < threesMax) {
                        newTile = new Tile(3, this.randDirection(), null, treasuresCount, false)
                        treasuresCount++
                        threesCount++
                    } else if (nextTile === 4 && foursCount < foursMax) {
                        if (randTreasures < randTreasureMax && foursCount % 2 == 0) {
                            newTile = new Tile(4, this.randDirection(), null, treasuresCount, false)
                            randTreasures++
                            treasuresCount++
                        } else {
                            newTile = new Tile(4, this.randDirection(), null, null, false)
                        }
                        foursCount++
                    }
                }

                if (!this.board[i]) this.board[i] = []
                this.board[i][j] = newTile
            }
        }
        var outerTile
        if (twosCount < twosMax) {
            outerTile = new Tile(2, this.randDirection(), null, null, false)
        }
        if (threesCount < threesMax) {
            outerTile = new Tile(3, this.randDirection(), null, treasuresCount, false)
        }
        if (foursCount < foursMax) {
            if (randTreasures < randTreasureMax) {
                outerTile = new Tile(4, this.randDirection(), null, treasuresCount, false)
            } else {
                outerTile = new Tile(4, this.randDirection(), null, null, false)
            }
        }
        this.outerTile = outerTile
    }

}