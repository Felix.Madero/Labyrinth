class Hand {
    constructor() {
        this.cards = []
        this.currentCard
    }

    drawCard() {
        this.currentCard = this.cards.pop()
    }

    addCard(card) {
        this.cards.push(card)
    }
}